import django
from django.db import models
from recycle import settings
import os
from recycle import settings

# Create your models here.

class MyUser(models.Model):
    """User table"""
    user = models.OneToOneField(django.contrib.auth.models.User)
    location = models.CharField(max_length=500, blank=True, null=True)
    seller_profile = models.OneToOneField('Seller', related_name='myuser')
    buyer_profile = models.OneToOneField('Buyer', related_name='myuser')
    favourites = models.ManyToManyField('Product', blank=True, null=True)

    def __unicode__(self):
        return '%s' % self.user.username

    @property
    def num_favorites(self):
        return self.favourites.count()

    @property
    def num_unread_msgs(self):
        return Message.objects.filter(message_to=self, status="unread").count()
    

class Seller(models.Model):
    """Seller profile"""
    credit = models.IntegerField()

    def __unicode__(self):
        return '%s' % self.myuser.user.username

    def check_enough_credit(self):
        return self.credit > 0

    def decr_credit(self):
        self.credit -= 1
        self.save()
        
class Buyer(models.Model):
    """Buyer profile"""
    def __unicode__(self):
        return '%s' % self.myuser.user.username

class Category(models.Model):
    MAIN_CATEGORIES = (('makeup', 'Make up'),
                       ('hair', 'hair'),
                       ('face', 'face'),
                       ('body', 'body'),
                       ('nails', 'nails'),
                       ('perfumery', 'perfumery'))
    name = models.CharField(max_length=500)
    main_category = models.CharField(max_length=500, choices=MAIN_CATEGORIES)
    image = models.ImageField(upload_to='.', default='no_image.png', blank=False, null=False)

    def __unicode__(self):
        return '%s [%s]' % (self.name, self.main_category)

    class Meta:
        verbose_name_plural = 'categories'

class Brand(models.Model):
    name = models.CharField(max_length=500)
    def __unicode__(self):
        return '%s' % (self.name)

class Product(models.Model):
    STATUS = (('active', 'active'),
              ('sold', 'sold'))

    name = models.CharField(max_length=500)
    price = models.CharField(max_length=500)
    amount_remaining = models.CharField(max_length=500)
    brand = models.ForeignKey('Brand')
    product_website = models.CharField(max_length=500)
    description = models.TextField()
    image = models.ImageField(upload_to='.',
                              default='no_image.png',
                              null=True, blank=True)
    category = models.ForeignKey('Category')
    seller = models.ForeignKey('Seller')
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=50, choices=STATUS, default='active')
    buyer = models.ForeignKey('Buyer', default=None, null=True, blank=True)

    def __unicode__(self):
        return u'%s [%s]' % (self.name, self.seller.myuser.user.username)

    @property
    def brand_name(self):
        return str(self.brand.name)

class SellerReview(models.Model):
    STATUS = (('pending', 'pending'),
              ('complete', 'complete'))
    product = models.ForeignKey('Product')
    status = models.CharField(max_length=50, choices=STATUS, default='pending')
    review_from = models.ForeignKey('MyUser',
                                    related_name='all_seller_reviews_made')
    review_to = models.ForeignKey('MyUser',
                                  related_name='all_seller_reviews_received')
    item_description_rating = models.FloatField(blank=True, null=True, default=0)
    communication_rating = models.FloatField(blank=True, null=True, default=0)
    shipping_speed_rating = models.FloatField(blank=True, null=True, default=0)
    shipping_charges_rating = models.FloatField(blank=True, null=True, default=0)
    seller_rating = models.FloatField(blank=True, null=True, default=0)
    comments = models.CharField(max_length=500, default="", null=True, blank=True)

    def __unicode__(self):
        return 'from [%s] to [%s] about [%s]' % (self.review_from, self.review_to, self.product.name)
    

class BuyerReview(models.Model):
    STATUS = (('pending', 'pending'),
              ('complete', 'complete'))
    product = models.ForeignKey('Product')
    status = models.CharField(max_length=50, choices=STATUS, default='pending')
    review_from = models.ForeignKey('MyUser',
                                    related_name='all_buyer_reviews_made')
    review_to = models.ForeignKey('MyUser',
                                  related_name='all_buyer_reviews_received')
    communication_rating = models.FloatField(blank=True, null=True, default=0)
    payment_speed_rating = models.FloatField(blank=True, null=True, default=0)
    buyer_rating = models.FloatField(blank=True, null=True, default=0)
    comments = models.CharField(max_length=500, default="", null=True, blank=True)

    def __unicode__(self):
        return 'from [%s] to [%s] about [%s]' % (self.review_from, self.review_to, self.product.name)
    
class Message(models.Model):
    STATUS = (('unread', 'unread'),
              ('read', 'read'))
    message_from = models.ForeignKey('MyUser', related_name='all_messages_to')
    message_to = models.ForeignKey('MyUser', related_name='all_messages_from')
    title = models.CharField(max_length=500)
    body = models.TextField()
    status = models.CharField(max_length=50, default='unread', choices=STATUS)
    created = models.DateTimeField(auto_now_add=True)

    @property
    def is_unread(self):
        return self.status == 'unread'
