from django import forms
import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class UserCreateForm(UserCreationForm):
    """Extend Django's UserCreationForm to include email address"""
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user

class EmailUpdateForm(forms.Form):
    new_email_1 = forms.CharField(label="New email")
    new_email_2 = forms.CharField(label="Confirm new email")

    def clean_new_email_2(self):
        email1 = self.cleaned_data.get('new_email_1')
        email2 = self.cleaned_data.get('new_email_2')
        if email1 and email2:
            if email1 != email2:
                raise forms.ValidationError("The two email fields didn't match")
        return email2

    def save(self,user):
        """Saves the email address"""
        user.email = self.cleaned_data['new_email_1']
        user.save()

class LocationUpdateForm(forms.Form):
    city = forms.CharField(label="City")
    state = forms.CharField(label="State")
    country = forms.CharField(label="Country")
    _zip = forms.CharField(label="Zip/Postal Code")
    
    def save(self, myuser):
        """Saves the location"""
        myuser.location = ', '.join([self.cleaned_data['city'],
                                     self.cleaned_data['state'],
                                     self.cleaned_data['country'],
                                     self.cleaned_data['_zip']])
        myuser.save()
        

    
class AddProductForm(forms.Form):
    category = forms.ChoiceField(label="Category",
                                 choices=[(c.id, str(c)) for c in models.Category.objects.all()])

    brand = forms.ChoiceField(label="Brand",
                              choices=[(b.id, str(b)) for b in models.Brand.objects.all()])
    
    full_product_name = forms.CharField(label="Full Product Name")
    amount_remaining = forms.CharField(label="Amount of Product Remaining")
    original_website = forms.CharField(label="Original Product Website (Showing Price)")
    selling_price = forms.CharField(label="Selling Price $")
    description = forms.CharField(label="Product Description",
                                  widget=forms.widgets.Textarea)
    image = forms.ImageField(
        label="Image (500KB limit)",
        help_text="Tip: Camera phone pictures are generally smaller in size.",
        required=False)

    def save(self, myuser):
        product = models.Product(name=self.cleaned_data['full_product_name'],
                                 price=self.cleaned_data['selling_price'],
                                 brand=models.Brand.objects.get(pk=self.cleaned_data['brand']),
                                 amount_remaining=self.cleaned_data['amount_remaining'],
                                 product_website=self.cleaned_data['original_website'],
                                 description=self.cleaned_data['description'],
                                 category=models.Category.objects.get(pk=self.cleaned_data['category']),
                                 image=self.cleaned_data['image'],
                                 seller=myuser.seller_profile)
        # save image in view
        product.save()
        myuser.seller_profile.decr_credit()
                                 

class SellerReviewForm(forms.Form):
    RATINGS = ((1, '1'),
               (2, '2'),
               (3, '3'),
               (4, '4'),
               (5, '5'))
    review_id = forms.IntegerField(widget=forms.HiddenInput)
    item_description = forms.ChoiceField(label="Overall Item Description", choices=RATINGS)
    communication = forms.ChoiceField(label="Overall Communication", choices=RATINGS)
    shipping_speed = forms.ChoiceField(label="Overall Shipping Speed", choices=RATINGS)
    shipping_charges = forms.ChoiceField(label="Overall Shipping Charges", choices=RATINGS)
    seller_rating = forms.ChoiceField(label="Overall Seller Rating", choices=RATINGS)
    comments = forms.CharField(max_length=2000, label="Comments", widget=forms.Textarea)

    def save(self):
        review = models.SellerReview.objects.get(pk=self.cleaned_data['review_id'])
        review.status = 'complete'
        review.item_description_rating = self.cleaned_data['item_description']
        review.communication_rating = self.cleaned_data['communication']
        review.shipping_speed_rating = self.cleaned_data['shipping_speed']
        review.shipping_charges_rating = self.cleaned_data['shipping_charges']
        review.seller_rating = self.cleaned_data['seller_rating']
        review.comments = self.cleaned_data['comments']
        review.save()

class BuyerReviewForm(forms.Form):
    RATINGS = ((1, '1'),
               (2, '2'),
               (3, '3'),
               (4, '4'),
               (5, '5'))
        
    review_id = forms.IntegerField(widget=forms.HiddenInput)
    communication = forms.ChoiceField(label="Overall Communication", choices=RATINGS)
    payment_speed = forms.ChoiceField(label="Overall Payment Speed", choices=RATINGS)
    buyer_rating = forms.ChoiceField(label="Overall Buyer Rating", choices=RATINGS)
    comments = forms.CharField(max_length=2000, label="Comments", widget=forms.Textarea)

    def save(self):
        review = models.BuyerReview.objects.get(pk=self.cleaned_data['review_id'])
        review.status = 'complete'
        review.communication_rating = self.cleaned_data['communication']
        review.payment_speed_rating = self.cleaned_data['payment_speed']
        review.buyer_rating = self.cleaned_data['buyer_rating']
        review.comments = self.cleaned_data['comments']
        review.save()
    

class SendMessageForm(forms.Form):
    to = forms.CharField(label="to")
    title = forms.CharField(label="title")
    body = forms.CharField(label="body", widget=forms.Textarea)

    def clean_to(self):
        try:
            to = models.MyUser.objects.get(user__username=self.cleaned_data['to'])
        except models.MyUser.DoesNotExist:
            raise forms.ValidationError('no such user')
        
        return self.cleaned_data['to']

    def save(self, myuser):
        to = models.MyUser.objects.get(user__username=self.cleaned_data['to'])
        message = models.Message.objects.create(message_from=myuser,
                                                message_to = to,
                                                title = self.cleaned_data['title'],
                                                body = self.cleaned_data['body'])
        
