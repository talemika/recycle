import django
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.db.models import Avg

import models
from forms import *

def register(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            # Create myuser
            seller = models.Seller(credit=0)
            seller.save()
            buyer = models.Buyer()
            buyer.save()
            myuser = models.MyUser(user=new_user, location="",
                                   seller_profile=seller,
                                   buyer_profile=buyer)
            myuser.save()
            return HttpResponseRedirect(reverse(home))
    else:
        form = UserCreateForm()
        
    return render(request, "registration/register.html",
                  {'form': form,})

def custom_proc(request):
    """A context processor that provides categories"""
    return {
        'categories': models.Category.objects.all(),
    }


def home(request):
    # create seller/buyer profiles if not created
        
    return render(request, "home.html", {},
                  context_instance=RequestContext(request, processors=[custom_proc]))

def faq(request):
    return render(request, "faq.html", {},
                  context_instance=RequestContext(request, processors=[custom_proc]))



@login_required
def account_home(request):
    """Home for user account information page"""
    return render(request, "account_info_home.html", {},
                  context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def user_profile(request):
    user = request.user
    myuser = models.MyUser.objects.get(user=user)
    return render_to_response("user_profile.html",
                              {'myuser': myuser},
                               context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def email_update(request):
    if request.method == "POST":
        form = EmailUpdateForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            messages.success(request, "Email updated successfully.")
            return HttpResponseRedirect(reverse(account_home))
    else:
        form = EmailUpdateForm()
        
    return render_to_response('email_update.html',
                              {'user': request.user,
                               'form':form},
                               context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def location_update(request):
    myuser = models.MyUser.objects.get(user=request.user)
    if request.method == "POST":
        form = LocationUpdateForm(request.POST)
        if form.is_valid():
            form.save(myuser)
            messages.success(request, "Location updated successfully.")
            return HttpResponseRedirect(reverse(account_home))
    else:
        form = LocationUpdateForm()
        
    return render_to_response('location_update.html',
                              {'myuser': myuser,
                               'form':form},
                            context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def selling_inventory(request):
    # Get all products that are selled by the user
    myuser = models.MyUser.objects.get(user=request.user)
    seller = myuser.seller_profile
    all_products = models.Product.objects.filter(seller=seller).exclude(status='sold')
    
    return render_to_response("selling_inventory.html",
                              {'all_products': all_products},
                               context_instance=RequestContext(request, processors=[custom_proc]))


def not_enough_credit(request):
    messages.error(request, "You do not have enough credit to list a product.")
    return HttpResponseRedirect(reverse(buy_tokens))

@login_required
def add_new_product(request):
    myuser = models.MyUser.objects.get(user=request.user)
    if not myuser.seller_profile.check_enough_credit():
        return not_enough_credit(request)
        
    if request.method == 'POST':
        form = AddProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(myuser)
            messages.success(request, "The product added successfully.")
            return HttpResponseRedirect(reverse(selling_inventory))
    else:
        form = AddProductForm()
        
    return render_to_response("add_product.html",
                              {'form': form},
                            context_instance=RequestContext(request, processors=[custom_proc]))

def check_product_ownership(product, myuser):
    """Check if the product belongs to the user <myuser>"""
    return product.seller == myuser.seller_profile

@login_required
def remove_product(request):
    product_id = request.POST['product_id']
    myuser = models.MyUser.objects.get(user=request.user)
    # get the product
    product = models.Product.objects.get(pk=product_id)
    # make sure user is authorized to remove this product
    if not check_product_ownership(product, myuser):
        messages.error(request, "You can remove your own products only.")
        return HttpResponseRedirect(reverse(selling_inventory))

    # else it is good to delete
    product.delete()
    messages.success(request, "The product removed successfully.")
    return HttpResponseRedirect(reverse(selling_inventory))

@login_required
def mark_product_sold(request):
    product_id = request.POST['product_id']
    buyer_username = request.POST['buyer_username']
    myuser = models.MyUser.objects.get(user=request.user)
    # get the product
    product = models.Product.objects.get(pk=product_id)
    try:
        buyer_user = models.MyUser.objects.get(user__username=buyer_username)
    except models.MyUser.DoesNotExist:
        messages.error(request, "User %s does not exist" % buyer_username)
        return HttpResponseRedirect(reverse(selling_inventory))
    # check if the product belongs to the user
    if not check_product_ownership(product, myuser):
        messages.error(request, "You can sell your own products only.")
        return HttpResponseRedirect(reverse(selling_inventory))
    # else it is good to mark as sold
    product.status = 'sold'
    product.buyer = buyer_user.buyer_profile
    product.save()

    # Create pending reviews
    models.BuyerReview.objects.create(review_from=myuser, review_to=buyer_user,
                                      product=product)
    models.SellerReview.objects.create(review_from=buyer_user, review_to=myuser,
                                       product=product)
    messages.success(request, "The product sold successfully.")
    return HttpResponseRedirect(reverse(selling_inventory))

@login_required
def sell_product_again(request):
    product_id = request.POST['product_id']
    product = models.Product.objects.get(pk=product_id)
    myuser = models.MyUser.objects.get(user=request.user)
    # Check if product belongs to the user
    if not check_product_ownership(product, myuser):
        messages.error(request, "You can sell your own products only.")
        return HttpResponseRedirect(reverse(selling_history))
    # Check if the user has enough credits
    if not myuser.seller_profile.check_enough_credit():
        return not_enough_credit(request)
    # Otherwise, it is good to sell again
    # copy the product and save as another instance
    product.status = 'active'
    product.buyer = None
    product.pk = None
    product.save()
    myuser.seller_profile.decr_credit()
    messages.success(request, "Product listed as for sale successfully.")
    return HttpResponseRedirect(reverse(selling_history))
    
@login_required
def selling_credit(request):
    myuser = models.MyUser.objects.get(user=request.user)
    seller = myuser.seller_profile
    return render_to_response("selling_credit.html",
                              {'seller': seller},
                            context_instance=RequestContext(request, processors=[custom_proc]))


@login_required
def buy_tokens(request):
    return render_to_response("buy_tokens.html",
                              {},
                               context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def selling_history(request):
    # Get all products that are sold
    myuser = models.MyUser.objects.get(user=request.user)
    seller = myuser.seller_profile
    all_products = models.Product.objects.filter(seller=seller, status='sold')
    return render_to_response("selling_history.html",
                              {'all_products': all_products},
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def pending_reviews(request):
    myuser = models.MyUser.objects.get(user=request.user)
    # get pending buyer reviews
    pending_buyer_reviews = models.BuyerReview.objects.filter(review_from=myuser,
                                                              status='pending')
    pending_seller_reviews = models.SellerReview.objects.filter(review_from=myuser,
                                                                status='pending')
                                                         
    return render_to_response("pending_reviews.html",
                              {'pending_buyer_reviews': pending_buyer_reviews,
                               'pending_seller_reviews': pending_seller_reviews,},
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def do_seller_review(request):
    def review_get():
        review_id = request.GET['seller_review_id']
        form = SellerReviewForm(initial={'review_id': review_id})
        return render_to_response("seller_review_form.html",
                                  {'form': form},
                                  context_instance=RequestContext(request, processors=[custom_proc]))

    def review_post():
        form = SellerReviewForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Review submitted successfully.")
            return HttpResponseRedirect(reverse(pending_reviews))
        
    if request.method == 'GET':
        return review_get()
    else:
        return review_post()
    
@login_required
def do_buyer_review(request):
    def review_get():
        review_id = request.GET['buyer_review_id']
        form = BuyerReviewForm(initial={'review_id': review_id})
        return render_to_response("buyer_review_form.html",
                                  {'form': form},
                                  context_instance=RequestContext(request, processors=[custom_proc]))
    def review_post():
        form = BuyerReviewForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Review submitted successfully.")
            return HttpResponseRedirect(reverse(pending_reviews))
    if request.method == "POST":
        return review_post()
    else:
        return review_get()

def seller_reviews(request, user_id):
    # get all seller reviews
    myuser = models.MyUser.objects.get(pk=user_id)
    seller_reviews = models.SellerReview.objects.filter(review_to=myuser,
                                                        status='complete')
    item_description = seller_reviews.aggregate(
        Avg('item_description_rating'))['item_description_rating__avg']
    communication = seller_reviews.aggregate(
        Avg('communication_rating'))['communication_rating__avg']
    shipping_speed = seller_reviews.aggregate(
        Avg('shipping_speed_rating'))['shipping_speed_rating__avg']
    shipping_charges = seller_reviews.aggregate(
        Avg('shipping_charges_rating'))['shipping_charges_rating__avg']
    seller_rating = seller_reviews.aggregate(
        Avg('seller_rating'))['seller_rating__avg']

    return render_to_response("seller_reviews.html",
                              {
                                  'item_description': item_description,
                                  'communication': communication,
                                  'shipping_speed': shipping_speed,
                                  'shipping_charges': shipping_charges,
                                  'seller_rating': seller_rating,
                                  'seller_reviews': seller_reviews,
                              },
                              context_instance=RequestContext(request, processors=[custom_proc]))
@login_required
def my_seller_reviews(request):
    myuser = models.MyUser.objects.get(user=request.user)
    return seller_reviews(request, myuser.pk)

def buyer_reviews(request, user_id):
    myuser = models.MyUser.objects.get(pk=user_id)
    buyer_reviews = models.BuyerReview.objects.filter(review_to=myuser,
                                                      status='complete')
    communication = buyer_reviews.aggregate(
        Avg('communication_rating'))['communication_rating__avg']
    payment_speed = buyer_reviews.aggregate(
        Avg('payment_speed_rating'))['payment_speed_rating__avg']
    buyer_rating = buyer_reviews.aggregate(
        Avg('buyer_rating'))['buyer_rating__avg']

    return render_to_response("buyer_reviews.html",
                              {
                                  'communication': communication,
                                  'payment_speed': payment_speed,
                                  'buyer_rating': buyer_rating,
                                  'buyer_reviews': buyer_reviews,
                              },
                              context_instance=RequestContext(request, processors=[custom_proc]))
@login_required
def my_buyer_reviews(request):
    myuser = models.MyUser.objects.get(user=request.user)
    return buyer_reviews(request, myuser.pk)

def view_category(request, category_id):
    category = models.Category.objects.get(pk=category_id)
    products = models.Product.objects.filter(category=category).all()
    return render_to_response("view_category.html",
                              {
                                  'category': category,
                                  'products': products},
                              context_instance=RequestContext(request, processors=[custom_proc]))
    
def view_product(request, product_id):
    product = models.Product.objects.get(pk=product_id)
    return render_to_response("view_product.html",
                              {'product': product},
                              context_instance=RequestContext(request, processors=[custom_proc]))
@login_required
def add_to_fav(request):
    product_id = request.POST['product_id']
    product = models.Product.objects.get(pk=product_id)
    myuser = models.MyUser.objects.get(user=request.user)
    myuser.favourites.add(product)
    myuser.save()
    messages.success(request, "Product added to your favourites successfully.")
    return HttpResponseRedirect(reverse('view_product', kwargs={'product_id': product_id}))

@login_required
def view_favourites(request):
    myuser = models.MyUser.objects.get(user=request.user)
    favourites = myuser.favourites.all()
    return render_to_response("view_favourites.html",
                              {'favourites': favourites},
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def send_message(request, to=None):
    myuser = models.MyUser.objects.get(user=request.user)
    if request.method == "POST":
        form = SendMessageForm(request.POST)
        if form.is_valid():
            form.save(myuser)
            messages.success(request, "Message sent successfully.")
            return HttpResponseRedirect(reverse(home))

    else:
        initials = {}
        if to:
            try:
                myuser = models.MyUser.objects.get(user__username=to)
                initials['to'] = myuser.user.username
            except RuntimeError as e:
                pass
        form = SendMessageForm(initial=initials)

    return render_to_response("send_message.html",
                              {'form': form},
                              context_instance=RequestContext(request, processors=[custom_proc]))
    
@login_required
def view_all_messages(request):
    myuser = models.MyUser.objects.get(user=request.user)
    in_messages = models.Message.objects.filter(message_to=myuser)
    out_messages = models.Message.objects.filter(message_from=myuser)
    return render_to_response("view_messages.html",
                              {'in_messages': in_messages,
                               'out_messages': out_messages,},
                              context_instance=RequestContext(request, processors=[custom_proc]))

@login_required
def view_message(request):
    message_id = request.POST['message_id']
    mark_as_read = request.POST['mark_as_read']
    message = models.Message.objects.get(pk=message_id)
    if mark_as_read=='True':
        message.status = 'read'
        message.save()
    return render_to_response("view_message.html",
                              {'message': message},
                              context_instance=RequestContext(request, processors=[custom_proc]))

def search(request):
    """Search a brand/product/description"""
    text = request.GET['search_text']
    search_results = []
    for p in models.Product.objects.filter(status='active'):
        if text in p.description or text in p.brand_name or text in p.name:
            search_results.append(p)

    return render_to_response("search_results.html",
                              {'products': search_results},
                              context_instance=RequestContext(request,
                                                              processors=[custom_proc]))
